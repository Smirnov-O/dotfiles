alias cls clea
alias cp "cp -r"
alias mkdir "mkdir -p"
alias py python
alias d docker
alias g git

# exa
alias ls "exa -l"
alias la "ls -a"
alias ll ls

# vim
alias vim nvim
alias v vim

# go
alias gor "go run"
alias gob "go build"
alias gog "go get"

# navigations
alias ... "cd ../.."
alias .3 "cd ../../.."
